<?php

class Import_PlaylistItemsMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import PlaylistItem nodes');

    // need to add softdependency for mixes when that is done
    $this->softDependencies = array('Import_Users', 'Import_Shows', 'Import_Episodes', 'Import_Mixes');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PlaylistItem ID (pss.id)',
                'alias' => 'pss'
                )
             ),
        MigrateDestinationNode::getKeySchema()
    );

    $query = db_select('proton_showsets', 'pss');
    $query->fields('pss', array('id', 'setorder', 'showdateid', 'showid', 'setorder', 'bioid'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('playlist_item');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'test');

    $this->addFieldMapping('field_playlist_type')
         ->defaultValue(14);
    
    $this->addFieldMapping('field_playlist_parent','showdateid')
         ->sourceMigration('Import_Episodes');

    $this->addFieldMapping('field_playlist_child','id')
         ->sourceMigration('Import_Mixes');
    
    $this->addFieldMapping('field_playlist_order', 'setorder');

  }

  public function prepareRow($row) {
  	 
  	$title = array();
  	$title[] = db_query("SELECT CONCAT(ps.name, ' (', DATE(psd.showdate), ')') as title FROM proton_showdates psd INNER JOIN proton_shows ps ON psd.showid = ps.id WHERE psd.id = :id LIMIT 1", array(':id'=>$row->showdateid))->fetchField();
  	$title[] = db_query("SELECT name FROM proton_bios WHERE id = :id LIMIT 1", array(':id'=>$row->bioid))->fetchField();
  	$title[] = db_query("SELECT CONCAT('Part ', setorder, ' - ', setname) from proton_showsets WHERE id = :id LIMIT 1", array(':id'=>$row->id))->fetchField();
  	
  	$row->test = implode(" - ", $title);
  	 
  }
  
}

