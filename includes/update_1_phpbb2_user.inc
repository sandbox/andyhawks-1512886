<?php

require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');

class Update_1_PHPBB2_UserMigration extends ProtonMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Update PHPBB2 Users Migration');
    $this->map = new MigrateSQLMap($this->machineName,
        array('user_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );
    $query = db_select('phpbb_users', 'pu')
             ->fields('pu', array('user_id', 'user_active', 'username', 'user_password', 'user_regdate', 'user_email' ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser();

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    // One good way to organize your mappings is in three groups - mapped fields,
    // unmapped source fields, and unmapped destination fields

    // Mapped fields

    $this->addFieldMapping('uid', 'user_id')
         ->dedupe('users', 'uid');        

    // Dedupe assures that value is unique. Use it when source data is non-unique.
    // Pass the Drupal table and column for determining uniqueness.
    $this->addFieldMapping('name', 'username')
         ->dedupe('users', 'name');        

    // The migrate module automatically converts date/time strings to UNIX timestamps.
    $this->addFieldMapping('status', 'user_active');

    // The migrate module automatically converts date/time strings to UNIX timestamps.
    $this->addFieldMapping('created', 'user_regdate');

    // TODO Add dedupe with custom replacement pattern.
    $this->addFieldMapping('mail', 'user_email')
         ->dedupe('users', 'mail');

    // Convert passwords from old md5
    $this->addFieldMapping('pass', 'user_password');

    // The migrate module automatically converts date/time strings to UNIX timestamps.
//    $this->addFieldMapping('field_phpbb2_regdate', 'user_regdate');

//    $this->addFieldMapping('field_phpbb2_id', 'user_id');

    // Instead of mapping a source field to a destination field, you can
    // hardcode a default value. You can also use both together - if a default
    // value is provided in addition to a source field, the default value will
    // be applied to any rows where the source field is empty or NULL.
    $this->addFieldMapping('roles')
         ->defaultValue(2);

    $this->addFieldMapping('is_new')
         ->defaultValue(1);

    // This is a shortcut you can use to mark several destination fields as DNM
    // at once
    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login',
      'timezone', 'language', 'picture'));

  }

  // Covnert old MD5 passwords from PHPBB2 to D7 passwords
  public function complete($user, stdClass $row) {

//    $account = user_load($user->uid);

    if ($user->uid) {

      // get original md5 password
      $oldpass = $row->user_password;

      // Lower than DRUPAL_HASH_COUNT to make the update run at a reasonable speed.
      $hash_count_log2 = 11;

      // rehash old password
//      $newpass = user_hash_password($oldpass, $hash_count_log2);

      // indicate updated password
      $newpass = $oldpass;

      // resave user password
      db_update('users')
        ->fields(array('pass' => $newpass))
        ->condition('uid', $user->uid)
        ->execute();

    }
  }

}

