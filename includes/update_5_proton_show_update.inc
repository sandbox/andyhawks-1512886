<?php

class Update_5_Proton_Show_UpdateMigration extends ProtonMigration {
  public function __construct() {
    parent::__construct();

    $this->description = t('PHPBB2 Users to Show Update');

    // You may optionally declare dependencies for your migration - other migrations
    // which should run first. In this case, terms assigned to our nodes and
    // the authors of the nodes should be migrated before the nodes themselves.
    $this->dependencies = array('Update_1_PHPBB2_User');

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.'
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_users', 'pu')
             ->fields('pu', array('phpbb2_id', 'name', 'thumb', 'abstract', 'biobody' ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('show');

    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    $this->addFieldMapping('revision_uid', 'phpbb2_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    // The migrate module automatically converts date/time strings to UNIX timestamps.
//    $this->addFieldMapping('field_phpbb2_regdate', 'user_regdate');

    $this->addFieldMapping('field_artist_name', 'name');
    $this->addFieldMapping('field_artist_abstract', 'abstract');
    $this->addFieldMapping('field_artist_bio', 'biobody');
    $this->addFieldMapping('field_artist_photo', 'thumb')
         ->arguments(array('file_function' => 'file_move', 'file_replace' => FILE_EXISTS_REPLACE));
    $this->addFieldMapping('field_artist_email2', 'email');

    // set node author to uid 1
    $this->addFieldMapping('author')
         ->defaultValue(1);

    // Mapped fields
    // The destination handler needs the nid to change - since the incoming data
    // has a source id, not a nid, we need to apply the original wine migration
    // mapping to populate the nid.
    $this->addFieldMapping('nid', 'phpbb2_id')
         ->sourceMigration('Update_1_PHPBB2_User');
  }

}
