<?php

class Import_Artist_FilesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Artist image files');

    $this->map = new MigrateSQLMap($this->machineName,
       array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Artist ID (proton_bios.id)',
                'alias' => 'pb'
                )
             ),
        MigrateDestinationFile::getKeySchema()
    );


    $query = db_select('proton_bios', 'pb')
             ->fields('pb', array('id', 'thumb'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

    // Just map the incoming URL to the destination's 'uri'
    $this->addFieldMapping('uri', 'thumb');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status', 'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    if ($row->thumb == '') {
      return false;
    }

    $row->thumb = 'http://protonradio.com' . $row->thumb;
  }

}

class Import_Artist_UsersMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Add referenced users to Artist role');

    $this->softDependencies = array('Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.',
		'alias' => 'pu'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );

    $query = db_select('proton_bios', 'pb');
    $query->leftJoin('proton_user_bios', 'pub', 'pb.id = pub.bio_id');
    $query->leftJoin('proton_users', 'pu', 'pub.user_id = pu.id');
    $query->fields('pu', array('id', 'phpbb2_id'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser;

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('roles')
         ->defaultValue(array(2, 4));

    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login', 'timezone', 'language', 'picture', 'is_new', 'mail', 'pass', 'created', 'status', 'name'));


  }

  public function prepare(stdClass $account, stdClass $row) {

    $roles = $account->roles;
    $roles[4] = true;
    $account->roles=$roles;
  }

}

class Import_ArtistsMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Artist nodes');

    $this->softDependencies = array('Import_Users', 'Import_Artist_Files', 'Import_Artist_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Artist ID (proton_bios.id)',
                'alias' => 'pb'
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_bios', 'pb');
    $query->leftJoin('proton_user_bios', 'pub', 'pb.id = pub.bio_id');
    $query->leftJoin('proton_users', 'pu', 'pub.user_id = pu.id');
    $query->fields('pb', array('id', 'name', 'thumb', 'abstract', 'biobody'));
    $query->fields('pu', array('phpbb2_id', 'accesslevel'));
    $query->addField('pu', 'id', 'pu_id');    

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('artist');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('field_artist_abstract', 'abstract');
    $this->addFieldMapping('body', 'biobody');
    $this->addFieldMapping('field_id_proton', 'id');

    $this->addFieldMapping('field_artist_photo', 'id')
         ->sourceMigration('Import_Artist_Files')
         ->arguments(array('file_function' => 'file_fid'));

    $this->addFieldMapping('field_artist_email2', 'email');

    $this->addFieldMapping('field_artist_users','accesslevel')
         ->sourceMigration('Import_Users');

  }

  public function prepareRow($row) {
    $result = db_query("SELECT COUNT(n.nid) as numrows from {node} n WHERE n.type = 'artist' AND n.title = :name", array(':name'=>$row->name));
    foreach ($result as $returnrow) {
      if ($returnrow->numrows > 0) {
        return false;
      }
    }

    $result = db_query('SELECT pu.phpbb2_id from proton_bios pb LEFT JOIN proton_user_bios pub ON pb.id = pub.bio_id LEFT JOIN proton_users pu ON pub.user_id = pu.id  WHERE pb.id = :id', array(':id'=>$row->id));
    $ids = array();
    foreach ($result as $purow) {
      $ids[] = $purow->phpbb2_id;
    }
         
    $row->accesslevel = $ids;

  }

}
