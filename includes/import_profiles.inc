<?php

class Import_Main_ProfilesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Main Profile');

    // You may optionally declare dependencies for your migration - other migrations
    // which should run first. In this case, terms assigned to our nodes and
    // the authors of the nodes should be migrated before the nodes themselves.
    $this->softDependencies = array('Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'User ID (proton_users.phpbb2_id)',
                'alias' => 'pu'
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_users', 'pu')
             ->fields('pu', array('phpbb2_id', 'id', 'firstname', 'lastname' ));
    $query->addJoin('LEFT', 'phpbb_users', 'pb', 'pu.phpbb2_id = pb.user_id');
    $query->addField('pb', 'user_regdate');

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationProfile2('main');

    // Mapped fields
    // The destination handler needs the nid to change - since the incoming data
    // has a source id, not a nid, we need to apply the original wine migration
    // mapping to populate the nid.
    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('revision_uid', 'user_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('field_id_phpbb2', 'user_id');
    $this->addFieldMapping('field_id_proton', 'id');
    $this->addFieldMapping('field_name_first', 'firstname');
    $this->addFieldMapping('field_name_last', 'lastname');

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('field_photo', 'field_birthday', 'field_gender', 'field_hear'));

  }

}

