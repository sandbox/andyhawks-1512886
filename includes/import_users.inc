<?php

require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');

class Import_UsersMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('user_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'User ID (phpbb_users.user_id)',
                'alias' => 'pb'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );
    $query = db_select('phpbb_users', 'pb');
    $query->leftJoin('proton_users', 'pu', 'pu.phpbb2_id = pb.user_id');
    $query->fields('pb', array('user_id', 'user_active', 'username', 'user_password', 'user_regdate', 'user_email' ));
    $query->fields('pu', array('id', 'phpbb2_id', 'name', 'thumb', 'abstract', 'biobody', 'country', 'address' ));
 
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser();

    // One good way to organize your mappings is in three groups - mapped fields,
    // unmapped source fields, and unmapped destination fields

    // Mapped fields

    // Dedupe assures that value is unique. Use it when source data is non-unique.
    // Pass the Drupal table and column for determining uniqueness.
    $this->addFieldMapping('name', 'username')
         ->dedupe('users', 'name');        

    // The migrate module automatically converts date/time strings to UNIX timestamps.
    $this->addFieldMapping('status', 'user_active');

    // The migrate module automatically converts date/time strings to UNIX timestamps.
    $this->addFieldMapping('created', 'user_regdate');

    // TODO Add dedupe with custom replacement pattern.
    $this->addFieldMapping('mail', 'user_email')
         ->dedupe('users', 'mail');

    // Convert passwords from old md5
    $this->addFieldMapping('pass', 'user_password');

    // Instead of mapping a source field to a destination field, you can
    // hardcode a default value. You can also use both together - if a default
    // value is provided in addition to a source field, the default value will
    // be applied to any rows where the source field is empty or NULL.
    $this->addFieldMapping('roles')
         ->defaultValue(2);

    $this->addFieldMapping('is_new')
         ->defaultValue(1);

    // This is a shortcut you can use to mark several destination fields as DNM
    // at once
    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login',
      'timezone', 'language', 'picture'));

  }

  public function prepare(stdClass $account, stdClass $row) {
    // The source fields are named address, city, state, and zip.
    // The destination fields are street, city, province, and postal_code.

    $lid = 0;
    $location = array();

    if ($row->address) {
      $location = geocode_user_location($row->address);

      if (!array_key_exists('lat', $location)) {
        $location = parse_user_location($row->address);
      }

      if (count($location)) {
        $lid = location_save($location);

        if ($lid > 0) {
          $account->field_location_home['und'][] = $location;
        }
      }
    }
  }

  // Covnert old MD5 passwords from PHPBB2 to D7 passwords
  public function complete($user, stdClass $row) {

    if ($user->uid) {

      // get original md5 password
      $oldpass = $row->user_password;

      // Lower than DRUPAL_HASH_COUNT to make the update run at a reasonable speed.
      $hash_count_log2 = 11;

      // rehash old password
//      $newpass = user_hash_password($oldpass, $hash_count_log2);

      // indicate updated password
      $newpass = $oldpass;

      // resave user password
      db_update('users')
        ->fields(array('pass' => $newpass))
        ->condition('uid', $user->uid)
        ->execute();
    }
  }
}

function geocode_user_location($user_location = array()) {

  $location = array();

//  $service_url = 'http://maps.google.com/maps/geo?output=json&key='. variable_get('location_geocode_us_google_apikey', '') .'&q=';
  $service_url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&key='. variable_get('location_geocode_us_google_apikey', '') .'&address=';
  $http_reply = drupal_http_request($service_url . urlencode($user_location));
  $json_reply = json_decode($http_reply->data);
  if (!is_object($json_reply) || $json_reply->status != "OK")  {
    return false;
  }

  $return_object = $json_reply->results[0];

  // Translate the values returned by the Google Geocoding API to values used
  // by the Geocode module.
  foreach ($return_object->address_components as $component) {
    switch ($component->types) {
      case in_array('postal_code',$component->types):
        $location['postal_code'] = $component->long_name;
        break;
      case in_array('country',$component->types):
        $location['country'] = strtolower($component->short_name);
        break;
      case in_array('administrative_area_level_1',$component->types):
        $location['province'] = $component->long_name;
        break;
      case in_array('administrative_area_level_2',$component->types):
        $province2 = $component->long_name;
        break;
      case in_array('locality',$component->types):
        $location['city'] = $component->long_name;
        break;
      case in_array('administrative_area_level_3',$component->types):
        $city2 = $component->long_name;
        break;
      case in_array('route',$component->types):
        $street2 = $component->long_name;
        break;
      case in_array('street_number',$component->types):
        $street1 = $component->long_name . " ";
        break;
      default:
        break;
    }
  }

  $location['street'] = $street1 . $street2;
  if ((!in_array('city', $location) || $location['city'] == '') && $city2 != '') {
    $location['city'] = $city2;
  }
  if ((!in_array('province', $location) || $location['province'] == '') && $province2 != '') {
    $location['province'] = $province2;
  }
  $location['lat'] = $return_object->geometry->location->lat;
  $location['lon'] = $return_object->geometry->location->lng;

  return $location;
}

function parse_user_location($user_location = array()) {

  $street = $user_location[0];
  $additional = '';
  if (array_key_exists(1, $user_location)) {
    $additional = $user_location[1];
  }

  $city = '';
  if (array_key_exists(2, $user_location)) {
    $arr_city = explode(", ", $user_location[2]);
    $city = $arr_city[0];
  }

  $state = '';
  $postal_code = '';
  if (array_key_exists(1, $arr_city)) {
    $arr_state = explode(" ", $arr_city[1]);
    $state = $arr_state[0];
    if (array_key_exists(1, $arr_state)) {
      $postal_code = $arr_state[1];
    }
  }

  $location = array(
          'street' => $street,
          'additional' => $additional,
          'city' => $city,
          'province' => $state,
          'postal_code' => $postal_code,
          'country' => $row->country,
          // There are many more available keys, see location_save or location module for more details
          );

  return $location;

}



