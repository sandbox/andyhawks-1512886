<?php

class Update_2_PHPBB2_Profile_MainMigration extends ProtonMigration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Update PHPBB2 Users to Main Profile2 Entities');

    // You may optionally declare dependencies for your migration - other migrations
    // which should run first. In this case, terms assigned to our nodes and
    // the authors of the nodes should be migrated before the nodes themselves.
    $this->dependencies = array('Update_1_PHPBB2_User');

    $this->map = new MigrateSQLMap($this->machineName,
        array('user_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.'
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('phpbb_users', 'pu')
             ->fields('pu', array('user_id', 'user_regdate' ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationProfile2('main');

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    // Mapped fields
    // The destination handler needs the nid to change - since the incoming data
    // has a source id, not a nid, we need to apply the original wine migration
    // mapping to populate the nid.
    $this->addFieldMapping('nid', 'user_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    $this->addFieldMapping('uid', 'user_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    $this->addFieldMapping('revision_uid', 'user_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    // The migrate module automatically converts date/time strings to UNIX timestamps.
//    $this->addFieldMapping('field_phpbb2_regdate', 'user_regdate');

    $this->addFieldMapping('field_id_phpbb2', 'user_id')
         ->sourceMigration('Update_1_PHPBB2_User');

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('id'));
  }

}

