<?php


class Import_Release_Files_CoverMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Release cover art image files');

    $this->map = new MigrateSQLMap($this->machineName,
       array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Release ID (proton_releases.id)',
                )
             ),
        MigrateDestinationFile::getKeySchema()
    );

    $query = db_select('proton_releases', 'pr')
             ->fields('pr', array('id', 'img_cover_art'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

    $this->addFieldMapping('uri', 'img_cover_art');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
      'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    $row->img_cover_art = 'http://protonradio.com/images/labels/covers/' . $row->img_cover_art;
  }

}

class Import_Release_Files_FlyerMigration extends ProtonMigration {

	public function __construct() {

		parent::__construct();
		$this->description = t('Import Release flyer image files');

		$this->map = new MigrateSQLMap($this->machineName,
				array('id' => array(
						'type' => 'int',
						'not null' => TRUE,
						'description' => 'Release ID (proton_releases.id)',
				)
				),
				MigrateDestinationFile::getKeySchema()
		);

		$query = db_select('proton_releases', 'pr')
		->fields('pr', array('id', 'img_flyer'));

		$this->source = new MigrateSourceSQL($query);
		$this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

		$this->addFieldMapping('uri', 'img_flyer');
		$this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
				'filemime', 'timestamp'));
	}

	public function prepareRow($row) {
		$row->img_flyer = 'http://protonradio.com/images/labels/flyers/' . $row->img_flyer;
	}

}

class Import_ReleasesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Release nodes');

    $this->softDependencies = array('Import_Release_Files_Flyer', 'Import_Release_Files_Cover', 'Import_Labels', 'Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Release ID (proton_releases.id)',
		'alias' => 'pr',
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_releases', 'pr');
    $query->leftJoin('proton_labels_releasejoin', 'plr', 'pr.id = plr.releaseid');
    $query->fields('pr', array('id', 'name', 'catalog', 'date', 'pending', 'cover_art_md5_checksum', 'description', 'upc', 'preorder', 'type', 'img_flyer', 'img_cover_art', 'beatport_exclusive_period' ));
    $query->fields('plr', array('labelid'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('release');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('field_release_catalog', 'catalog');
    $this->addFieldMapping('field_release_date', 'date');
    $this->addFieldMapping('field_release_leagacy_id', 'id');
    $this->addFieldMapping('field_release_cover_art_checksum', 'cover_art_md5_checksum');
    $this->addFieldMapping('field_release_upc', 'upc');
    $this->addFieldMapping('field_release_preorder', 'preorder');
    $this->addFieldMapping('field_release_beatport_period', 'beatport_exclusive_period');
    $this->addFieldMapping('field_release_type', 'type');
    $this->addFieldMapping('status', 'pending');

    $this->addFieldMapping('field_release_cover_art', 'img_cover_art')
         ->sourceMigration('Import_Release_Files_Cover')
         ->arguments(array('file_function' => 'file_fid'));

    $this->addFieldMapping('field_release_flyer', 'img_flyer')
    ->sourceMigration('Import_Release_Files_Flyer')
    ->arguments(array('file_function' => 'file_fid'));
    
    // managers goes here - this neesd to map toartist nid
    $this->addFieldMapping('field_release_label','labelid')
         ->sourceMigration('Import_Labels');

  }

  public function prepareRow($row) {

    if ($row->pending == 0) {
      $row->pending = 1;
    } else {
      $row->pending = 0;
    }
    
  	switch ($row->type) {
      case "1":
        $row->type = "Single";
        break;
      case "2":
        $row->type = "EP";
        break;
      case "3":
        $row->type = "Mini Album";
        break;
      case "4":
        $row->type = "Album";
        break;
    }
    
    $query = new EntityFieldQuery;
    $result = $query
    ->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('name', $row->type)
    ->propertyCondition('vid', 9)
    ->execute();
    foreach ($result['taxonomy_term'] as $term) {
    	$row->type = $term->tid;
    }
    
  }

}
