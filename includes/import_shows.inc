<?php

class Import_Show_FilesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Show image files');

    $this->map = new MigrateSQLMap($this->machineName,
       array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.'
                )
             ),
        MigrateDestinationFile::getKeySchema()
    );

    $query = db_select('proton_shows', 'ps')
             ->fields('ps', array('id', 'thumb'));
    $this->source = new MigrateSourceSQL($query);

    $this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

    $this->addFieldMapping('uri', 'thumb');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
      'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    $row->thumb = 'http://protonradio.com/' . $row->thumb;
  }

}

class Import_Show_Host_UsersMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Add referenced hosts users to Shows role');

    $this->softDependencies = array('Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.',
                'alias' => 'pu'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );

    $query = db_select('proton_shows', 'ps');
    $query->leftJoin('proton_user_bios', 'pub', 'ps.hostbioid = pub.bio_id');
    $query->leftJoin('proton_users', 'pu', 'pub.user_id = pu.id');
    $query->fields('pu', array('id', 'phpbb2_id'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser;

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('roles')
         ->defaultValue(array(2, 4, 5));

    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login', 'timezone', 'language', 'picture', 'is_new', 'mail', 'pass', 'created', 'status', 'name'));

  }

  public function prepare(stdClass $account, stdClass $row) {
   
    $roles = $account->roles;
    $roles[5] = true;
    $account->roles=$roles;
  }

}

class Import_Show_Manager_UsersMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Add referenced manager users to Shows role');

    $this->softDependencies = array('Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.',
                'alias' => 'pu'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );

    $query = db_select('proton_shows', 'ps');
    $query->leftJoin('proton_user_host', 'puh', 'ps.id = puh.showid');
    $query->leftJoin('proton_user_bios', 'pub', 'puh.bioid = pub.bio_id');
    $query->leftJoin('proton_users', 'pu', 'pub.user_id = pu.id');
    $query->fields('pu', array('id', 'phpbb2_id'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser;

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('roles')
         ->defaultValue(array(2, 4, 5));

    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login', 'timezone', 'language', 'picture', 'is_new', 'mail', 'pass', 'created', 'status', 'name'));

  }

  public function prepare(stdClass $account, stdClass $row) {

    $roles = $account->roles;
    $roles[5] = true;
    $account->roles=$roles;
  }

}

class Import_ShowsMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Show nodes');

    $this->softDependencies = array('Import_Users', 'Import_Show_Files', 'Import_Show_Host_Users', 'Import_Show_Manager_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Show ID (ps.id)',
                'alias' => 'ps'
                )
             ),
        MigrateDestinationNode::getKeySchema()
    );

    $query = db_select('proton_shows', 'ps');
    $query->leftJoin('proton_user_host', 'puh', 'ps.id = puh.showid');
    $query->fields('ps', array('id', 'hostbioid', 'name', 'summary', 'thumb', 'hostbioid', 'email', 'dir', 'weekday', 'timeofday', 'id', 'showmode', 'length'));
    $query->fields('puh', array('bioid'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('show');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('body', 'summary');
    $this->addFieldMapping('field_show_email', 'email');
    $this->addFieldMapping('field_show_directory', 'dir');
    $this->addFieldMapping('field_show_legacy_id', 'id');

    $this->addFieldMapping('field_show_icon', 'id')
         ->sourceMigration('Import_Show_Files')
         ->arguments(array('file_function' => 'file_fid'));

    // host goes here - this neesd to map toartist nid
    $this->addFieldMapping('field_show_host','hostbioid')
         ->sourceMigration('Import_Artists');

    // managers goes here - this neesd to map toartist nid
    $this->addFieldMapping('field_show_managers','bioid')
         ->sourceMigration('Import_Artists');

    // timeslot function, handled in prepareRow
    $this->addFieldMapping('field_show_timeslot','timeofday');

  }

  public function prepareRow($current_row) {

    // ignore existing rows
    $result = db_query("SELECT COUNT(n.nid) as numrows from {node} n WHERE n.type = 'show' AND n.title = :name", array(':name'=>$current_row->name));
    foreach ($result as $returnrow) {
      if ($returnrow->numrows > 0) {
        return false;
      }
    }

    // get array of manager ids
    $ids = array();
    $result = db_query('SELECT puh.bioid from proton_shows ps LEFT JOIN proton_user_host puh ON ps.id = puh.showid WHERE ps.id = :id', array(':id'=>$current_row->id));
    foreach ($result as $purow) {
      $ids[] = $purow->bioid;
    }
    $row->bioid = $ids;

    // date transformations
    $strday = "Sunday";
    $byday = "SU";
    switch ($current_row->weekday) {
      case "1":
        $strday = "Sunday";
        $byday = "SU";
        break;
      case "2":
        $strday = "Monday";
        $byday = "MO";
        break;
      case "3":
        $strday = "Tuesday";
        $byday = "TU";
        break;
      case "4":
        $strday = "Wednesday";
        $byday = "WE";
        break;
      case "5":
        $strday = "Thursday";
        $byday = "TH";
        break;
      case "6":
        $strday = "Friday";
        $byday = "FR";
        break;
      case "7":
        $strday = "Saturday";
        $byday = "SA";
        break;
    }

    $timeofday = $current_row->timeofday / 60;
    $time = explode(".",$timeofday);
    $strtime = $time[0] . ":";
    if (isset($time[1])) { 
      $time[1] = ($time[1] / 10) * 60; 
    } else {
      $time[1] = "00";
    } 
    $strtime .= $time[1];

    $strstart = "first";
    $bydaywk = "+1".$byday;
    $freq = "MONTHLY";
    switch ($current_row->showmode) {
      case 1: //every week
        $freq = "WEEKLY";
        break;
      case 3: //every 1st wk monthly
        break;
      case 4: //every 2nd wk monthly
        $strstart = "second";
        $bydaywk = "+2".$byday;
        break;
      case 5: //every 3rd wk monthly
        $strstart = "third";
        $bydaywk = "+3".$byday;
        break;
      case 6: //every 4th wk monthly
        $strstart = "fourth";
        $bydaywk = "+4".$byday;
        break;
      case 7: //every 1st and 3rd monthly
        $bydaywk = "+1".$byday.",+3".$byday;
        break;
      case 8: //every 2nd and 4th monthly
        $bydaywk = "+2".$byday.",+4".$byday;
        break;
      case 10: //hiatus
        break;
      case 2: // every other - not used
      case 9: // every last - not used
      case 11: // every quarter - not used
        break;
    }

    date_default_timezone_set('UTC');

    $firstshow_results = db_query('SELECT showdate from proton_showdates psd WHERE showid = :id ORDER BY showdate ASC LIMIT 1', array(':id'=>$current_row->id));
    if ($firstshow_results) {
      foreach ($firstshow_results as $purow) {
        $timestamp_from = strtotime($purow->showdate);
      }
    } else {
      $firstshow = date("F d Y",strtotime("now -1 year")) . "T235959Z";
      $date_from_string = $firstshow . ' ' . $strstart . ' ' . $strday . $strtime;
      $timestamp_from = strtotime($date_from_string);
    }
    $from = date("m/d/Y H:i:s",$timestamp_from);
    $timestamp_to = $timestamp_from + ($current_row->length * 60);
    $to = date('m/d/Y H:i:s', $timestamp_to); 
  
    $lastshow = date("Ymd",strtotime("now +1 year")) . "T235959Z";
    $lastshow_results = db_query('SELECT showdate from proton_showdates psd WHERE showid = :id ORDER BY showdate DESC LIMIT 1', array(':id'=>$current_row->id));
    foreach ($lastshow_results as $purow) {
      if ($current_row->showmode == '10') {
        $lastshow = date("Ymd",strtotime($purow->showdate)). "T235959Z";
      } else {
        $enddate = strtotime($purow->showdate . " +1 year");
        $lastshow = date("Ymd",$enddate). "T235959Z";
      }
    }

    $rrule = "RRULE:FREQ=".$freq.";UNTIL=".$lastshow.";WKST=SU;BYDAY=".$bydaywk;

    if ($current_row->showmode == '11') {
      $from = '';
      $to = '';
      $rrule = '';
    }
    $date_data = array(
      'from' => (string)$from,
      'to' => (string)$to,
      'rrule' => (string)$rrule,
      'timezone' => 'America/New_York',
      'timezone_db' => 'UTC'
    );

    $current_row->timeofday = drupal_json_encode($date_data);
    return $current_row;
  }
}

