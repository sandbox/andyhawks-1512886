<?php

class Import_EpisodesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Episode nodes');

    // need to add softdependency for mixes when that is done
    $this->softDependencies = array('Import_Users', 'Import_Shows');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Episode ID (psd.id)',
                'alias' => 'psd'
                )
             ),
        MigrateDestinationNode::getKeySchema()
    );

    $query = db_select('proton_showdates', 'psd');
    $query->fields('psd', array('id', 'showid', 'showdate'));
    $query->leftJoin('proton_shows', 'ps', 'ps.id = psd.showid');
    $query->fields('ps', array('name'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('episode');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'name');

    // host goes here - this neesd to map toartist nid
    $this->addFieldMapping('field_episode_show','showid')
         ->sourceMigration('Import_Shows');

    $this->addFieldMapping('field_episode_date', 'showdate');
    $this->addFieldMapping('field_episode_legacy_id', 'id');

  }

  public function prepareRow($current_row) {

    $date = split(" ",$current_row->showdate);
    $current_row->name = $current_row->name . " " . $date[0]; 

    // mix stuff will go here

    return $current_row;
  }
}

