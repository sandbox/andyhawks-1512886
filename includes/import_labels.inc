<?php


class Import_Label_FilesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Label image files');

    $this->map = new MigrateSQLMap($this->machineName,
       array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Label ID (proton_labels.id)',
                )
             ),
        MigrateDestinationFile::getKeySchema()
    );

    $query = db_select('proton_labels', 'pl')
             ->fields('pl', array('id', 'icon'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

    $this->addFieldMapping('uri', 'icon');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
      'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    $row->icon = 'http://protonradio.com/images/labels/label_icons/' . $row->icon;
  }

}

class Import_Label_UsersMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Add referenced manager users to Labels role');

    $this->softDependencies = array('Import_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('phpbb2_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'PHPBB2 User ID.',
                'alias' => 'pu'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );

    $query = db_select('proton_labels', 'pl');
    $query->leftJoin('proton_labels_managers', 'plm', 'pl.id = plm.labelid');
    $query->leftJoin('proton_user_bios', 'pub', 'plm.bioid = pub.bio_id');
    $query->leftJoin('proton_users', 'pu', 'pub.user_id = pu.id');
    $query->fields('pu', array('id', 'phpbb2_id'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser;

    // we set this so that re-migrations (updates) only update mapped fields and leave unmapped fields  as-is
    $this->systemOfRecord = Migration::DESTINATION;

    $this->addFieldMapping('uid', 'phpbb2_id')
         ->sourceMigration('Import_Users');

    $this->addFieldMapping('roles')
         ->defaultValue(array(2, 4, 5, 6));

    $this->addUnmigratedDestinations(array('theme', 'signature', 'access', 'login', 'timezone', 'language', 'picture', 'is_new', 'mail', 'pass', 'created', 'status', 'name'));

  }

  public function prepare(stdClass $account, stdClass $row) {

    $roles = $account->roles;
    $roles[6] = true;
    $account->roles=$roles;
  }

}

class Import_LabelsMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Label nodes');

    $this->softDependencies = array('Import_Users', 'Import_Label_Files', 'Import_Label_Users');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Label ID (proton_labels.id)',
		'alias' => 'pl',
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_labels', 'pl');
    $query->leftJoin('proton_labels_managers', 'plm', 'pl.id = plm.labelid');
    $query->fields('pl', array('id', 'name', 'type_id', 'active', 'dir', 'icon' ));
    $query->fields('plm', array('bioid'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('label');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('field_label_legacy_id', 'id');
    $this->addFieldMapping('field_label_type', 'type_id');
    $this->addFieldMapping('field_label_active', 'active');
    $this->addFieldMapping('field_label_directory', 'dir');

    $this->addFieldMapping('field_label_icon', 'id')
         ->sourceMigration('Import_Label_Files')
         ->arguments(array('file_function' => 'file_fid'));

    // managers goes here - this neesd to map toartist nid
    $this->addFieldMapping('field_label_managers','bioid')
         ->sourceMigration('Import_Artists');

  }

  public function prepareRow($row) {

    // ignore existing rows
    $result = db_query("SELECT COUNT(n.nid) as numrows from {node} n WHERE n.type = 'label' AND n.title = :name", array(':name'=>$row->name));
    foreach ($result as $returnrow) {
      if ($returnrow->numrows > 0) {
        return false;
      }
    }

    // get array of manager ids
    $ids = array();
    $result = db_query('SELECT plm.bioid FROM proton_labels_managers plm WHERE plm.labelid = :id', array(':id'=>$row->id));
    foreach ($result as $purow) {
      $ids[] = $purow->bioid;
    }
    $row->bioid = $ids;



    if ($row->icon != '') {
      $row->icon = $_SERVER['DOCUMENT_ROOT'] . '/' . conf_path() . '/files' . $row->icon;
    }
    switch ($row->type_id) {
      case "1":
        $row->type_id = "1st party";
        break;
      case "2":
        $row->type_id = "2nd party";
        break;
      case "3":
        $row->type_id = "3rd party";
        break;
    }
    
    $query = new EntityFieldQuery;
    $result = $query
      ->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('name', $row->type_id)
      ->propertyCondition('vid', 8)
      ->execute();  
    foreach ($result['taxonomy_term'] as $term) {
    	$row->type_id = $term->tid;
    }  
  }

}
