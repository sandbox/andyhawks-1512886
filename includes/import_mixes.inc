<?php

/*
class Import_Mix_Files_96Migration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Mix 96kbps files');

    $this->map = new MigrateSQLMap($this->machineName,
       array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Mix ID (proton_showsets.id)',
                )
             ),
        MigrateDestinationFile::getKeySchema()
    );

    $query = db_select('proton_showsets', 'ps')
             ->fields('ps', array('id', 'file_96'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

    $this->addFieldMapping('uri', 'file_96');
    $this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
      'filemime', 'timestamp'));
  }

  public function prepareRow($row) {
    $row->img_cover_art = 'http://protonradio.com/audio/96/' . $row->file_96;
  }

}
*/
class Import_Mix_Files_192Migration extends ProtonMigration {

	public function __construct() {

		parent::__construct();
		$this->description = t('Import Mix 192kbps files');

		$this->map = new MigrateSQLMap($this->machineName,
				array('id' => array(
						'type' => 'int',
						'not null' => TRUE,
						'description' => 'Mix ID (proton_showsets.id)',
				)
				),
				MigrateDestinationFile::getKeySchema()
		);

		$query = db_select('proton_showsets', 'ps')
		->fields('ps', array('id', 'setlocation'));

		$this->source = new MigrateSourceSQL($query);
		$this->destination = new MigrateDestinationFile(array('copy_file' => TRUE));

		$this->addFieldMapping('uri', 'setlocation');
		$this->addUnmigratedDestinations(array('fid', 'uid', 'filename', 'status',
				'filemime', 'timestamp'));
	}

	public function prepareRow($row) {
		$row->img_cover_art = 'http://protonradio.com/audio/192/' . $row->file_192;
	}

}

class Import_MixesMigration extends ProtonMigration {

  public function __construct() {

    parent::__construct();
    $this->description = t('Import Mix nodes');

    $this->softDependencies = array('Import_Mix_Files_192', 'Import_Artists', 'Import_Shows', 'Import_Episodes');

    $this->map = new MigrateSQLMap($this->machineName,
        array('id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'Mix ID (proton_showsets.id)',
		'alias' => 'ps',
                )
             ),
        MigrateDestinationProfile2::getKeySchema()
    );

    $query = db_select('proton_showsets', 'ps');
    $query->fields('ps', array('id', 'showid', 'showdateid', 'bioid', 'isondemand', 'setname', 'seconds' ));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('mix');

    $this->addFieldMapping('uid')
         ->defaultValue(1);
    $this->addFieldMapping('revision_uid')
         ->defaultValue(1);

    $this->addFieldMapping('title', 'test');
    $this->addFieldMapping('field_mix_name', 'setname');
    $this->addFieldMapping('field_release_catalog', 'catalog');
    $this->addFieldMapping('field_release_date', 'date');
    $this->addFieldMapping('field_release_cover_art_checksum', 'cover_art_md5_checksum');
    $this->addFieldMapping('field_release_upc', 'upc');
    $this->addFieldMapping('field_release_preorder', 'preorder');
    $this->addFieldMapping('field_mix_length', 'seconds');
    $this->addFieldMapping('field_mix_legacy_id', 'id');
    $this->addFieldMapping('field_mix_legacy_artist_id', 'bioid');
    $this->addFieldMapping('field_mix_legacy_show_id', 'showid');
    $this->addFieldMapping('field_mix_legacy_episode_id', 'showdateid');
    $this->addFieldMapping('status', 'isondemand');
    

    $this->addFieldMapping('field_mix_file_low', 'file_96')
         ->sourceMigration('Import_Mix_Files_96')
         ->arguments(array('file_function' => 'file_fid'));

    $this->addFieldMapping('field_mix_file', 'file_192')
         ->sourceMigration('Import_Mix_Files_192')
         ->arguments(array('file_function' => 'file_fid'));
    
    $this->addFieldMapping('field_mix_artist','bioid')
         ->sourceMigration('Import_Artists');
    
  }

  public function prepareRow($row) {
  	
  	$title = array();
  	$title[] = db_query("SELECT CONCAT(ps.name, ' (', DATE(psd.showdate), ')') as title FROM proton_showdates psd INNER JOIN proton_shows ps ON psd.showid = ps.id WHERE psd.id = :id LIMIT 1", array(':id'=>$row->showdateid))->fetchField();
  	$title[] = db_query("SELECT name FROM proton_bios WHERE id = :id LIMIT 1", array(':id'=>$row->bioid))->fetchField();
  	$title[] = db_query("SELECT CONCAT('Part ', setorder, ' - ', setname) from proton_showsets WHERE id = :id LIMIT 1", array(':id'=>$row->id))->fetchField();
  		 
  	$row->test = implode(" - ", $title);
  	  	 
  }

  public function prepare(stdClass $node, stdClass $row) {
  
  	$node->field_mix_length['und'][0]['hour'] = intval(intval($row->seconds) / 3600);
  	$node->field_mix_length['und'][0]['minute'] = intval(($row->seconds / 60) % 60);
  	$node->field_mix_length['und'][0]['second'] =  intval($row->seconds % 60);

  }
  
  
}
